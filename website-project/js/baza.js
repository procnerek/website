var autor = 
[
{autor: "mlaval"},
{autor: "maxime1992"},
{autor: "filipesilva"},
{autor: "gdi2290"},
{autor: "merzak7"},
{autor: "jeffbcross"},
{autor: "juliemr"},
{autor: "topherfangio"},
{autor: "petebacondarwin"}
];

var data = 
[
	{
		name: '<a href="https://github.com/angular/angular">angular</a>',
		date: "18/09/2016" ,	
		autor: '<a href="https://github.com/mlaval" title="Ostatni współtwórca">mlaval</a>', 
		stars: 16324  ,
		text: " "
	},
	{
		name: '<a href="https://github.com/angular/angular.io">angular.io</a>',
		date: "18/09/2016" ,	
		autor: '<a href="https://github.com/maxime1992" title="Ostatni współtwórca">maxime1992</a>', 
		stars: 598  ,
		text: "Website for Angular 2" + '</br>'
	},
	{
		name: '<a href="https://github.com/angular/angular-cli">angular-cli</a>',
		date: "18/09/2016" ,	
		autor: '<a href="https://github.com/filipesilva" title="Ostatni współtwórca">filipesilva</a>', 
		stars: 3673  ,
		text: "CLI tool for Angular2" + '</br>'
	},
	{
		name: '<a href="https://github.com/angular/universal">universal</a>',
		date: "18/09/2016" ,	
		autor: '<a href="https://github.com/gdi2290" title="Ostatni współtwórca">gdi2290</a>', 
		stars: 1300  ,
		text: "Universal (isomorphic) javascript support for Angular2" + '</br>'
	},
	{
		name: '<a href="https://github.com/angular/material">material</a>',
		date: "17/09/2016" ,	
		autor: '<a href="https://github.com/merzak7" title="Ostatni współtwórca">merzak7</a>', 
		stars:  14479  ,
		text: "Material design for Angular" + '</br>'
	},
	{
		name: '<a href="https://github.com/angular/zone.js">zone.js</a>',
		date: "17/09/2016" ,	
		autor: '<a href="https://github.com/jeffbcross" title="Ostatni współtwórca">jeffbcross</a>', 
		stars: 1651  ,
		text: "Implements Zones for JavaScript" + '</br>'
	},
	{
		name: '<a href="https://github.com/angular/protractor">protractor</a>',
		date: "16/09/2016" ,	
		autor: '<a href="https://github.com/juliemr" title="Ostatni współtwórca">juliemr</a>', 
		stars: 15841 ,
		text: "E2E test framework for Angular apps" + '</br>'
	},
	{
		name: '<a href="https://github.com/angular/material-start">material-start</a>',
		date: "14/09/2016" ,	
		autor: '<a href="https://github.com/topherfangio" title="Ostatni współtwórca">topherfangio</a>', 
		stars: 1868    ,
		text: "Quick Starter Repository for Angular Material" + '</br>'
	},
	{
		name: '<a href="https://github.com/angular/angular-phonecat">angular-phonecat</a>',
		date: "14/09/2016" ,	
		autor: '<a href="https://github.com/petebacondarwin" title="Ostatni współtwórca">petebacondarwin</a>', 
		stars: 2632  ,
		text: "Tutorial on building an angular application." + '</br>'
	},
];
